# ISSUE: HTTPS DOES NOT WORK 

HTTPS requests (git clone, wget/curl) are observed to fail in butch. 
For example,

	$ butch install efivar-git  &
	$ tail -f /src/logs/build_efivar-git.log 
	...
	failure "fatal: unable to access 'https://github.com/rhinstaller/efivar.git/': Could not resolve host: github.com

The same requests will succeed outside butch however. This is likely
because a package that needs network for the build (eg. git clone)
doesn't specify "need_net=1" in its [vars]. Fix the package recipe and
maybe submit a change to the sabotage repo.

# ISSUE: "TERMINAL IS NOT FULLY FUNCTIONAL"

In the chroot environment, less, man, etc. say "WARNING: terminal is not fully
functional". Using TERM=linux or TERM=rxvt seems to fix this.

# ISSUE: CAPS LOCK LED DOES NOT WORK

On Thinkpad X1 Carbon Gen 7 (at least) the Caps Lock LED does not
turn on and off when the key is toggled. This seemed to be a transient
issue, fixed by a reboot. It consistently works as expected now.
There are a few related askubuntu/stack exchange questions that may 
be helpful if this doesn't resolve the issue. TODO(ns): add those links.

# ISSUE: LID CLOSE DOES NOT SUSPEND

TODO(ns): add notes.
