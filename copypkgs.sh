set -eo pipefail

sabotagerepo=/src/sabotage

# returns 0 if file is tracked in git.
tracked() {
	> /dev/null 2>&1 git -C "$sabotagerepo" ls-files --error-unmatch "$1"
}

# copies a file from this repo to the official sabotage packages dir.
# usage: c <$dst> <$src>
#        c <$file>
c() {
	local d=$1 # destination path in sabotage repo
	local s=$2 # source file
	[ -z "$s" ] && s=$1

	if tracked "$d"; then
		echo 2>&1 "$s -> $d: replacing official sabotage path of same name"  
	fi
	cp -a "$s" /src/"$d"
}

c pkg/kernel510x
c pkg/kernel510x-patch
c pkg/kernel510x-tarball
c KEEP/kernelx.config
c pkg/cryptsetup2
